
#include <stdio.h>

int population_count(unsigned long mask) {
    int count;
    for(count = 0;mask != 0ul;count++)
        mask &= mask - 1ul;
    return count;
}

int contains_number(int mask, int number) {
    return mask & 1<<number; 
}

int calc_diff(int mask, int * size_of_sums, int * size_of_diff) {
    int mask_for_diffs_pos = 0, mask_for_diffs_neg = 0;
    
    for(int i=0; i<population_count(mask); i++) {
        for(int j=0; j<population_count(mask); j++) { // diff is not sommutative, so we need to go through all numbers again
            if(i-j >= 0) {
                if( (mask_for_diffs_pos & (i-j)) == 0 )
                    mask_for_diffs_pos += 1<<(i-j); // if sum not present in set add it
            }
            else {
                if( (mask_for_diffs_neg & (-(i-j)) ) == 0 )
                    mask_for_diffs_neg += 1<<(-(i-j)); // if sum not present in set add it
            }
        }    
    }

    return population_count(mask_for_diffs_pos) + population_count(mask_for_diffs_neg); // return number of sums
}

int calc_sums(int mask, int * size_of_sums, int * size_of_diff) {
    int mask_for_sums = 0;
    
    for(int i=0; i<population_count(mask); i++) {
        for(int j=i; j<population_count(mask); j++) { // sum is sommutative, so no need to go through all numbers again
            if( (mask_for_sums & (i+j)) == 0 )
                mask_for_sums += 1<<(i+j); // if sum not present in set add it
        }    
    }

    return population_count(mask_for_sums); // return number of sums
}

int solve_subset (int size, int * size_of_sums, int * size_of_diff, int mask) {
    for(int i=0; i<size; i++) { // add each number to the subset at the time
        if(contains_number(mask, i)) // if number is already in the subset or set contains less than 2 elements, continue
            continue;
        mask += 1<<i; // add number to the set
        if(population_count(mask) < 2)
            continue;

        int n_sums = calc_sums(mask, size_of_sums, size_of_diff), n_diffs = calc_diff(mask, size_of_sums, size_of_diff);
        // printf("Size: %d; # sums: %d: # diffs: %d\n", size, n_sums, n_diffs);
        if(n_sums > n_diffs) {
            // printf("Mask: %d; # sums: %d; # diffs: %d\n", mask, n_sums, n_diffs);
            return 1; // if # of sums > # of diffs return 1
        }

        solve_subset(size, size_of_sums, size_of_diff, mask); // recurse
    }

    return 0; 
}

int solve_subset_stats (int size, int * size_of_sums, int * size_of_diff, int mask, int stats[]) {
    // printf("# subset: %d\n", population_count(mask));
    for(int i=0; i<size; i++) { // add each number to the subset at the time
        if(contains_number(mask, i)) // if number is already in the subset or set contains less than 2 elements, continue
            continue;
        mask += 1<<i; // add number to the set
        if(population_count(mask) < 2)
            continue;

        int n_sums = calc_sums(mask, size_of_sums, size_of_diff), n_diffs = calc_diff(mask, size_of_sums, size_of_diff);

        if (n_sums < stats[0]) stats[0] = n_sums;
        if (n_sums > stats[1]) stats[1] = n_sums;
        if (n_diffs < stats[2]) stats[2] = n_diffs;
        if (n_diffs > stats[3]) stats[3] = n_diffs;
        if ((n_sums - n_diffs) < stats[4]) stats[4] = (n_sums - n_diffs);
        if ((n_sums - n_diffs) > stats[5]) stats[5] = (n_sums - n_diffs);

        // // printf("Size: %d; # sums: %d: # diffs: %d\n", size, n_sums, n_diffs);
        // if(n_sums > n_diffs) {
        //     // printf("Mask: %d; # sums: %d; # diffs: %d\n", mask, n_sums, n_diffs);
        //     return 1; // if # of sums > # of diffs return 1
        // }

        if(population_count(mask) > size || solve_subset_stats(size, size_of_sums, size_of_diff, mask, stats)) // if subset contains all numbers 
            return 1;

        // solve_subset_stats(size, size_of_sums, size_of_diff, mask, stats); // recurse
    }

    return 0; 
}

int main () {
    int N = 100; //max size of sets
    
    // printf("Max size of sets: %d\n", N);

    int n;
    int subset_size;

    int size_of_sums[1<<N], size_of_diff[1<<N]; // store size of sum and diff subsets based on mask
    int mask = 0;

    for (n = 2; n <= N; n++) {
        if(solve_subset(n, size_of_sums, size_of_diff, mask) == 1)
            break;            
    }

    // Desired value is stored in n
    printf("The smallest value: %d\n", n);

    n = 32;
    mask = 0;
    int stats[6]; // stats for solve_subset_stats 0 - min sums, 1 - max sums, 2 - min diffs, 3 - max diffs, 4 - min sums-diffs, 5 - max sums-diffs
    stats[0] = 32*32; // min sums
    stats[1] = 0; // max sums
    stats[2] = 32*32; // min diffs
    stats[3] = 0; // max diffs
    stats[4] = 32*32; // min sums-diffs
    stats[5] = 0; // max sums-diffs
    solve_subset_stats(n, size_of_sums, size_of_diff, mask, stats);

    printf("Stats for n = 32:\n");
    printf("Min number of sums: %d\n", stats[0]);
    printf("Max number of sums: %d\n", stats[1]);
    printf("Min number of diffs: %d\n", stats[2]);
    printf("Max number of diffs: %d\n", stats[3]);
    printf("Min number of sums-diffs: %d\n", stats[4]);
    printf("Max number of sums-diffs: %d\n", stats[5]);
}